const functions = require('firebase-functions');
const admin = require('firebase-admin');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(functions.config().api.sendgrid);
const cors = require('cors')({ origin: true });
admin.initializeApp();

const db = admin.firestore();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.sendMail = functions.region('europe-west1').https.onRequest((req, res) => {
    cors(req, res, () => {

        const msgData = {
            from: req.body.sender,
            subject: req.body.subject,
            text: req.body.content
        };


        const msg = {
            to: 'arnodubois@sweetpunk.com',
            ...msgData,
        };

        db.collection('formResults').add({
            ...msgData
        })
        return sgMail.send(msg).then(() => {
            return res.send('OK');
        }).catch((err) => res.send(err));
    });
});
